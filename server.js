require('dotenv').config();
const config = require('./src/configs');
const mongoose = require('./src/services/mongoose.service');
const app = require('./src/services/express.service');

app.start(config.server.port);
mongoose.connect(config.database.mongo.url);