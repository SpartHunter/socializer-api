const Joi = require('joi');

module.exports = {
    create: {
        password: Joi.string().regex(/^[a-zA-Z0-9]{8,30}/).required(),
        email: Joi.string().email().required(),
        firstname: Joi.string().regex(/^[A-Z][a-z\s]/).required(),
        lastname: Joi.string().regex(/^[A-Z][a-z\s]/).required(),
        admin: Joi.string().regex(/^[A-Z][a-z\s]/).required(),
    }
};