const express = require('express');
const router = express.Router();
const user = require('../controllers/auth.controller');

// register a user
router.post('/auth/register', user.register);

// verify
router.get('/auth/sign', user.sign);

// login
router.post('/auth/login', user.login);

// logout
router.get('/auth/logout', user.logout);

module.exports = router;
